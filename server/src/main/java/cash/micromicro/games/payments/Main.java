package cash.micromicro.games.payments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.zarbosoft.asyncaws.AWS;
import com.zarbosoft.checkjson.Valid;
import com.zarbosoft.coroutines.Cohelp;
import com.zarbosoft.coroutinescore.SuspendExecution;
import com.zarbosoft.coroutinescore.SuspendableRunnable;
import com.zarbosoft.gettus.Cogettus;
import com.zarbosoft.gettus.Gettus;
import com.zarbosoft.gettus.GettusBase;
import com.zarbosoft.microwebserver.*;
import com.zarbosoft.scheduledxnio.ScheduledXnioWorker;
import io.undertow.Undertow;
import io.undertow.util.HttpString;
import io.undertow.websockets.core.WebSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xnio.OptionMap;
import org.xnio.Options;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static com.zarbosoft.coroutines.Cohelp.block;
import static com.zarbosoft.microwebserver.WebServer.jackson;
import static com.zarbosoft.microwebserver.WebServer.websocketSend;
import static com.zarbosoft.rendaw.common.Common.now;
import static com.zarbosoft.rendaw.common.Common.uncheck;
import static io.undertow.util.Headers.CONTENT_TYPE;
import static io.undertow.util.Methods.POST;
import static java.time.temporal.ChronoUnit.HOURS;

public class Main {
	private static final String ENV_MICROMICRO_HOST = System.getenv("MICROMICRO_HOST");
	private static final String ENV_MICROMICRO_USER = System.getenv("MICROMICRO_USER");
	private static final String ENV_MICROMICRO_TOKEN = System.getenv("MICROMICRO_TOKEN");
	private static final String ENV_MICROMICRO_SECRET = System.getenv("MICROMICRO_SECRET");

	private static final Logger logger = LoggerFactory.getLogger("main");
	private static final AtomicLong rate = new AtomicLong(100);
	private static ScheduledXnioWorker worker;
	private static final ConcurrentHashMap<String, NotifyInfo> notify = new ConcurrentHashMap<>();
	private static final Map<String, GameInfo> games = uncheck(() -> {
		JavaType infoType = jackson.getTypeFactory().constructParametricType(Map.class, String.class, GameInfo.class);
		return (Map<String, GameInfo>) jackson.readValue(Main.class
				.getClassLoader()
				.getResourceAsStream("cash/micromicro/games/payments/games.json"), infoType);
	});
	private static final AtomicInteger wsNextId = new AtomicInteger(0);

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class GameInfo {
		@JsonProperty
		public String name;

		@JsonProperty
		public long price;

		@JsonProperty
		public String wallet;
	}

	public static class NotifyInfo {
		public final Instant stamp = Instant.now();
		public final WebSocketChannel channel;
		public final GameInfo game;
		public final String address;

		public NotifyInfo(final WebSocketChannel channel, final GameInfo game, String address) {
			this.channel = channel;
			this.game = game;
			this.address = address;
		}
	}

	public static void main(final String[] args) {
		final int maxUserThreads = 16;
		final int maxInternalThreads = 4;
		Main.worker = uncheck(() -> ScheduledXnioWorker.createWorker(OptionMap
				.builder()
				.set(/* All user callbacks. */ Options.WORKER_TASK_MAX_THREADS, maxUserThreads)
				.set(/* Undertow (?) and xnio internal work. */ Options.WORKER_IO_THREADS, maxInternalThreads)
				.getMap()));
		try {
			block(() -> {
				Main.mainAsync(Main.worker);
			});
		} catch (final Throwable e) {
			Main.worker.shutdown();
			try {
				Main.worker.awaitTermination();
			} catch (final InterruptedException e1) {
			}
			throw e;
		}
	}

	public static void dbInc(String game, long amount) throws SuspendExecution {
		final String region = "us-west-2";
		AWS.send(worker.inner,
				region,
				POST,
				"dynamodb",
				String.format("dynamodb.%s", region),
				"/",
				ImmutableMap.<HttpString, String>builder()
						.put(CONTENT_TYPE, "application/x-amz-json-1.0")
						.put(HttpString.tryFromString("X-Amz-Target"), "DynamoDB_20120810.UpdateItem")
						.build(),
				null,
				JSONBuilder.bytes(JSONBuilder.on(o -> o
						.v("TableName", "H00000167467f1698")
						.o("Key",
								o2 -> o2
										.o("date", o3 -> o3.v("S", now().toLocalDate().toString()))
										.o("game", o3 -> o3.v("S", game))
						)
						.o(
								"AttributeUpdates",
								o2 -> o2.o("value",
										o3 -> o3.v("Action", "ADD").o("Value", o4 -> o4.v("N", Long.toString(amount)))
								)
						)))
		).body().check();
	}

	public static void mainAsync(final ScheduledXnioWorker worker) throws SuspendExecution {
		dbInc("test", 3);
		final SuspendableRunnable updatePrices = () -> {
			Main.rate.set(new Cogettus(worker.inner,
					Gettus.formatURI("https://api.%s/v1/rates", Main.ENV_MICROMICRO_HOST)
			)
					.host()
					.send()
					.body()
					.json()
					.get("usd")
					.asLong());
			Main.logger.info(String.format("Updated USD rate: %s", Main.rate.get()));
		};
		updatePrices.run();
		Cohelp.repeat(worker, 24, HOURS, updatePrices);
		Cohelp.repeat(worker, 1, HOURS, () -> {
			Instant start = Instant.now().minus(1, HOURS);
			notify.entrySet().removeIf(e -> {
				if (!e.getValue().stamp.isBefore(start))
					return false;
				uncheck(() -> e.getValue().channel.close());
				return true;
			});
		});

		final Undertow outward = new WebServer(null, worker, "outward", ImmutableList.of())
				.route(RequestHealth.class, "/health")
				.route(RequestAddress.class, "/address", true)
				.route(RequestCallback.class, String.format("/callback/%s", Main.ENV_MICROMICRO_SECRET))
				.start(worker.inner, 8080, "0.0.0.0");
		outward.start();

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				outward.stop();
				worker.shutdown();
				while (true)
					try {
						worker.awaitTermination();
						break;
					} catch (final InterruptedException e) {
					}
			}
		});
	}

	public static class RequestHealth extends Request<Main> {
		static ResponseRaw resp = new ResponseRaw(null, "salut".getBytes(StandardCharsets.UTF_8));

		@Override
		public Response process(
				final HttpString method, final Main context, final WebSocketChannel websocket
		) throws SuspendExecution {
			return RequestHealth.resp;
		}
	}

	public static class RequestAddress extends Request<Main> {
		@JsonProperty
		@Valid(min = Valid.Limit.INCLUSIVE, minValue = 1)
		public String game;

		@Override
		public Response process(
				final HttpString method, final Main context, final WebSocketChannel websocket
		) throws SuspendExecution {
			final GameInfo gameInfo = Main.games.get(this.game);
			if (gameInfo == null)
				throw ErrorRequest.format("Unknown game %s", this.game);
			String wsId = Integer.toString(wsNextId.getAndIncrement());
			final JsonNode addr = new Cogettus(context.worker.inner,
					Gettus.formatURI("https://api.%s/v1/new_in", Main.ENV_MICROMICRO_HOST)
			)
					.host()
					.method(POST)
					.bodyJson(b -> {
						b.writeStartObject();
						b.writeStringField("username", Main.ENV_MICROMICRO_USER);
						b.writeStringField("token", Main.ENV_MICROMICRO_TOKEN);
						b.writeStringField("tos", "latest");
						b.writeBooleanField("slow", false);
						b.writeBooleanField("single_use", true);
						b.writeStringField("receiver_message", wsId);
						b.writeNumberField("expire", Instant.now().plus(1, HOURS).toEpochMilli());
						b.writeStringField("sender_message", String.format("1 token for %s", gameInfo.name));
						b.writeNumberField("amount", context.rate.get() * gameInfo.price);
						b.writeEndObject();
					})
					.send()
					.body()
					.json();
			if (addr.has("error")) {
				Main.logger.error(String.format("Address creation error: %s", addr.get("error")));
				throw ErrorRequest.format("Internal error");
			}
			logger.debug(uncheck(() -> jackson.writeValueAsString(addr)));
			String mmId = addr.get("id").asText();
			Main.notify.put(wsId, new NotifyInfo(websocket, gameInfo, mmId));
			logger.debug(String.format("post address; notify is %s\n", notify));
			return new ResponseRaw(null,
					String
							.format("{\"address\":\"https://%s/app/#in/%s\"}", Main.ENV_MICROMICRO_HOST, mmId)
							.getBytes(StandardCharsets.UTF_8)
			);
		}
	}

	public static class RequestCallback extends RequestFreeJson<Main> {
		@Override
		public Response process(
				final HttpString method, final Main context, final WebSocketChannel _
		) throws SuspendExecution {
			do {
				if (this.data.has("testing"))
					break;
				logger.debug(uncheck(() -> jackson.writeValueAsString(data)));
				final String id = this.data.get("message").asText();
				logger.debug(String.format("pre callback; notify is %s\n", notify));
				final NotifyInfo notifyInfo = Main.notify.get(id);
				if (notifyInfo == null) {
					Main.logger.error(String.format("Unknown payment address %s", id));
					break;
				}
				Main.notify.remove(id);
				Cohelp.submit(Main.worker.inner, () -> {
					websocketSend(Main.worker.inner, notifyInfo.channel, ResponseEmpty.instance);
					uncheck(() -> notifyInfo.channel.sendClose());
				});
				Cohelp.submit(Main.worker.inner, () -> {
					Main.logger.info("Made payment: %s", new Cogettus(Main.worker.inner,
							GettusBase.formatURI("https://api.%s/v1/send", Main.ENV_MICROMICRO_HOST)
					).host().bodyJson(b -> {
						b.writeStartObject();
						b.writeStringField("username", Main.ENV_MICROMICRO_USER);
						b.writeStringField("token", Main.ENV_MICROMICRO_TOKEN);
						b.writeStringField("tos", "latest");
						b.writeStringField("dest", notifyInfo.game.wallet);
						b.writeNumberField("amount", this.data.get("amount").asLong());
						b.writeStringField("sender_message", "Forward " + notifyInfo.game.name);
						b.writeEndObject();
					}).send().body().check().text());
				});
			} while (false);
			return ResponseEmpty.instance;
		}
	}
}